package ru.t1.azarin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.model.Session;

import java.util.List;

public interface ISessionService {

    @NotNull
    Session add(@Nullable String userId, @Nullable Session session);

    void clear(@Nullable String userId);

    boolean existById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<Session> findAll(@Nullable String userId);

    @Nullable
    Session findOneById(@Nullable String userId, @Nullable String id);

    void removeById(@Nullable String userId, @Nullable String id);

}
